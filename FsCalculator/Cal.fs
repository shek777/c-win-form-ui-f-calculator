﻿/// Simple Module / Namespace
namespace FsCalculator

/// Simple Calculator done in F#
type Calculate = 
    /// Divide Method / Function, 
    /// this Method uses less lines of code than the Mod method. It uses branching statements and not pattern matching like the Mod Function.
    static member Divide x y = 
        if y = 0.0 then 0.0
        else x / y
    /// Multiply Method / Function
    static member Multiply x y = x * y
    /// Minus Method / Function
    static member Minus x y = x - y
    /// Add Method / Function
    static member Add x y = x + y
    /// Modulus Method / Function
    static member Mod x y = 
        match y with
        | 0.0 -> 0.0
        | _ -> x % y
    /// Exponentiation Method / Function
    static member Pow x y = x**y

