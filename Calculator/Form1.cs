﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Forms;
using Calculator.Properties;
using FsCalculator;

namespace Calculator
{
    public partial class Calculate : Form
    {
        public Calculate()
        {
            InitializeComponent();
        }

       

        private void btnMul_Click(object sender, EventArgs e)
        {
            Results.Text = FsCalculator.Calculate.Multiply((int)numericUpDownX.Value, (int)numericUpDownY.Value).ToString(CultureInfo.InvariantCulture);

           
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            try
            {
                Results.Text =
                    FsCalculator.Calculate.Divide((double) numericUpDownX.Value, (double) numericUpDownY.Value).ToString(CultureInfo.InvariantCulture);
               
            }
// ReSharper disable once EmptyGeneralCatchClause
            catch
            {
                Results.Text = Resources.Calculate_btnDiv_Click_Error__divide_by_zero_;
            }
          
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Results.Text = FsCalculator.Calculate.Add((int)numericUpDownX.Value, (int)numericUpDownY.Value).ToString(CultureInfo.InvariantCulture);
           
        }

        private void btnSub_Click(object sender, EventArgs e)
        {
            Results.Text = FsCalculator.Calculate.Minus((int)numericUpDownX.Value, (int)numericUpDownY.Value).ToString(CultureInfo.InvariantCulture);

           
        }

        private void btnMod_Click(object sender, EventArgs e)
        {
            try
            {

                Results.Text =
                    FsCalculator.Calculate.Mod((double) numericUpDownX.Value, (double) numericUpDownY.Value).ToString();
                
            }
// ReSharper disable once EmptyGeneralCatchClause
            catch
            {
                Results.Text = Resources.Calculate_btnDiv_Click_Error__divide_by_zero_;
            }
           
        }

        private void btnPow_Click(object sender, EventArgs e)
        {
            Results.Text =
                   FsCalculator.Calculate.Pow((double)numericUpDownX.Value, (double)numericUpDownY.Value).ToString();

        }

        private void btnClr_Click(object sender, EventArgs e)
        {
            numericUpDownX.Value = 0;
            numericUpDownY.Value = 0;
            Results.Text = "Answer                                   ";
        }
    }
}
